package uros.konjo.android.konjo;



/**
 * Created by Corto on 5/23/2016.
 */
public class Pawn extends Figure {

    public Pawn(String name){
        this.name=name;
        this.moveable=false;
        this.picture_figure=R.drawable.pawn;
    }

    public Pawn(String name, int cur_pos){
        this.name=name;
        this.moveable=false;
        this.current_position=cur_pos;
        this.picture_figure=R.drawable.pawn;
    }
    @Override
    public boolean isMoveable() {
        return moveable;
    }

    @Override
    protected void makeValideMoves() {
        //pawn is not movable
    }


}
