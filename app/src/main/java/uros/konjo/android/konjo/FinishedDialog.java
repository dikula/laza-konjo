package uros.konjo.android.konjo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Lazar Vasiljevic on 6/21/2016.
 */
public class FinishedDialog extends DialogFragment implements View.OnClickListener{

    private static final String ARG_MOVES="num_moves";
    private static final String ARG_NEXT_LEVEL="show_next_level_button";
    private static final String ARG_TITLE="set_title";
    public static final String EXTRA_ACTION="result_from_dialog";
    public static final int TAG_FINISH=1;
    public static final int TAG_RESTART=2;
    public static final int TAG_HIGHSCORE=3;
    public static final int TAG_NEXT_LEVEL =4;
    private int mMoves;
    private boolean mShowNextLevel;
    private String mTitle;
    private Button mFinish, mRestart,mHighscore, mNextLevel;


    public static FinishedDialog newInstance(int moves,boolean next_level, String title) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_MOVES, moves);
        args.putSerializable(ARG_NEXT_LEVEL,next_level);
        args.putSerializable(ARG_TITLE,title);
        FinishedDialog fragment = new FinishedDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_finish,null);
        mMoves=(int)getArguments().getSerializable(ARG_MOVES);
        mShowNextLevel=(boolean)getArguments().getSerializable(ARG_NEXT_LEVEL);
        mTitle=(String)getArguments().getSerializable(ARG_TITLE);
        TextView tv =(TextView) v.findViewById(R.id.dialog_label);
        tv.setText(mTitle + getResources().getString(R.string.gameeover_dialog_label) + String.valueOf(mMoves));
        mFinish = (Button) v.findViewById(R.id.dialog_finish_btn);
        mFinish.setTag(TAG_FINISH);
        mFinish.setOnClickListener(this);
        mRestart = (Button) v.findViewById(R.id.dialog_restart_btn);
        mRestart.setTag(TAG_RESTART);
        mRestart.setOnClickListener(this);
        mHighscore = (Button) v.findViewById(R.id.dialog_highscore_btn);
        mHighscore.setTag(TAG_HIGHSCORE);
        mHighscore.setOnClickListener(this);
        mNextLevel = (Button) v.findViewById(R.id.dialog_nextlevel_btn);
        mNextLevel.setTag(TAG_NEXT_LEVEL);
        mNextLevel.setOnClickListener(this);
        if(!mShowNextLevel){
            mNextLevel.setEnabled(mShowNextLevel);
        }
        return new AlertDialog.Builder(getActivity()).setTitle(mTitle).setView(v).create();
    }

    @Override
    public void onClick(View v) {
        sendResult(Activity.RESULT_OK,(int)v.getTag());
        dismiss();

    }

    private void sendResult(int resultCode, int action) {
        if (getTargetFragment() == null) {
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(EXTRA_ACTION, action);
        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
    }
}
