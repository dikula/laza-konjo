package uros.konjo.android.konjo;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import org.json.JSONArray;


public class MainActivity extends AppCompatActivity implements AsyncResponse{
    private final String TAG_ACTIVITY="Main_Activity";
    private static final String DIALOG_SETTINGS="FinishedLevel";

    private Fragment f;
    private FragmentManager fm;

    private  String mUsername;

    public String SETTINGS_PREF="user_settings";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         fm = getSupportFragmentManager();
         f = fm.findFragmentById(R.id.fragment_container);
        if(f == null){
            f = new StartFragment();
            fm.beginTransaction().add(R.id.fragment_container,f).commitNow();
        }

        SharedPreferences  prefs = this.getSharedPreferences(SETTINGS_PREF, Context.MODE_PRIVATE);
        setUsername(prefs.getString("username", ""));
        if(getUsername().isEmpty()){
            FragmentManager fm = getSupportFragmentManager();
            UserSettingsDialog dialog = UserSettingsDialog.newInstance(getUsername());
            dialog.show(fm,DIALOG_SETTINGS);
        }

    }

    @Override
    public void onBackPressed() {
        Log.d(TAG_ACTIVITY,"Back, handling");
        FragmentManager fm = getSupportFragmentManager();
        fm.saveFragmentInstanceState(f);
            super.onBackPressed();

    }
    public void startGame(View v){
        fm = getSupportFragmentManager();
        Fragment f = new MainActivityFragment();
        FragmentTransaction ft =fm.beginTransaction().replace(R.id.fragment_container,f);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_user_settings:
                FragmentManager fm = getSupportFragmentManager();
                UserSettingsDialog dialog = UserSettingsDialog.newInstance(getUsername());
                dialog.show(fm,DIALOG_SETTINGS);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public  void showHighScore(View v) {
        GetRequest gr = new GetRequest();
        gr.delegate=this;
        gr.execute("http://198.211.126.94:8111/get",null,null);

    }

    @Override
    public void answerFromServer(JSONArray jsonArray) {
        fm = getSupportFragmentManager();
        Fragment highscore = HighscoreFragment.newInstance(jsonArray,"START");
        FragmentTransaction ft = fm.beginTransaction().replace(R.id.fragment_container, highscore);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
    }


    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }
}
