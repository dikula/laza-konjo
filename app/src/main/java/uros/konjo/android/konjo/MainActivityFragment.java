package uros.konjo.android.konjo;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageButton;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements View.OnClickListener, AsyncResponse{

    private final String LOG_FRAGMENT ="Fragment";
    private final String LOG_LEVEL="Level";
    private static final String DIALOG_FINISHED="FinishedLevel";
    private static final int REQUEST_FINISH = 0;
    private Table table;
    private static int displayWidth;
    private static int displayHeight;
    private static boolean isFirst=true;
    private int last_pos;
    private int mCurrentLevel=1;
    private Konj konjo;
    private List<Figure> mEnemyFigure;
    private GridLayout grid;
    private Button statusButton;
    private static int moves = 0;
    private Vibrator vibrator;
    private Context context;
    private Level mLevel;

    public MainActivityFragment() {
        Log.d(LOG_FRAGMENT,"koristimo singlton za pravljenje table");
        table = Table.makeTable();
        mEnemyFigure=new ArrayList<>();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(LOG_FRAGMENT,"onCreateView");
        if(savedInstanceState!=null) {
            Log.d(LOG_FRAGMENT,"save from destroy");
            moves=savedInstanceState.getInt("moves");
        }

        View v = inflater.inflate(R.layout.fragment_main, container, false);
//        v.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION  | View.SYSTEM_UI_FLAG_FULLSCREEN);
        vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        if(!vibrator.hasVibrator()){
            vibrator=null;
        }

        statusButton = (Button) v.findViewById(R.id.status_label);

        updateStatus(moves);
        Point p= new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(p);
        displayHeight=p.y;
        displayWidth=p.x;

        grid = (GridLayout) v.findViewById(R.id.table_grid_layout);

        Log.d(LOG_FRAGMENT, "sirina " + displayWidth + "px visina " + displayHeight);
        drawTable(grid);

        return v;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();

    }
    public void drawTable(GridLayout grid){
        for(int i=0; i<table.getSize();i++){
            //Create ImageButton
            ImageButton im = new ImageButton(context);
            im.setBackgroundColor(get_color_field(i % table.getWidth(), i / table.getWidth()));
            im.setImageResource(table.getField_pic(i));
            im.setOnClickListener(this);
            im.setId(i);
            GridLayout.LayoutParams params = new GridLayout.LayoutParams();
            if(hasNavBar(getResources())){
                params.height = displayHeight/8 - 40;
            }else {
                params.height = displayHeight / 8 - 24;
            }
            params.width= displayWidth/4-20;
            params.setGravity(Gravity.CENTER);
            params.setMargins(4, 4, 4, 4);
            params.columnSpec = GridLayout.spec(i%table.getWidth());
            params.rowSpec = GridLayout.spec(i/table.getWidth());
            im.setLayoutParams(params);
            grid.addView(im);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_FINISH) {
            int action = (int) data.getSerializableExtra(FinishedDialog.EXTRA_ACTION);
            switch (action){
                case FinishedDialog.TAG_FINISH:
                    getActivity().finish();
                    break;
                case FinishedDialog.TAG_RESTART:
                    restartGame();
                    break;
                case FinishedDialog.TAG_HIGHSCORE:
                    GetRequest gr = new GetRequest();
                    gr.delegate=this;
                    gr.execute("http://198.211.126.94:8111/get",null,null);
                    restartGame();
                    break;
                case FinishedDialog.TAG_NEXT_LEVEL :
                    nextLevel();
                    mLevel=new Level(getLevel(),last_pos);
                     mEnemyFigure=new ArrayList<>(mLevel.getFigure());
                    break;
                default:
                    Log.e(LOG_FRAGMENT,"wrong return result");
            }
        }
    }

    @Override
    public void onResume () {
        super.onResume();
        Log.d(LOG_FRAGMENT, "onResume");
    }

    @Override
    public void onDestroy() {
        Log.d(LOG_FRAGMENT, " back button pressed");
        cleanTable();
        isFirst=true;
        moves=0;
        super.onDestroy();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("moves", moves);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(LOG_FRAGMENT, "onActivityCreated");
        if(savedInstanceState!=null) {
            Log.d(LOG_FRAGMENT, "not null");
            moves = savedInstanceState.getInt("moves");

        }
    }

    private int isPlaceCoverd(int pos){
        for (int i=0 ;i<mEnemyFigure.size();i++) {
            if(mEnemyFigure.get(i).isValidMove(pos)){
                return i;
            }
        }
        return -1;
    }

    private int isPositionOfEnemy(int pos){
        for (int i=0 ;i<mEnemyFigure.size();i++) {
            if(mEnemyFigure.get(i).getCurrent_position()==pos){
                return i;
            }
        }
        return -1;
    }

    private void eatKonjo(int pos,int enemy){
        for(int i=0; i<mEnemyFigure.size();i++) {
            table.setField(mEnemyFigure.get(enemy).getCurrent_position(), null);
            mEnemyFigure.get(enemy).setCurrent_position(pos);
            table.setField(pos, mEnemyFigure.get(enemy));
        }
    }
    @Override
    public void onClick(View v) {
        Log.d(LOG_FRAGMENT, "click " + v.getId());
        if(isFirst) {
            try{
                vibrator.vibrate(150);
            }catch (NullPointerException e){
                Log.e(LOG_FRAGMENT,"No vibrattion");
            }
            konjo = new Konj("konjo", v.getId());
            table.setField(v.getId(), konjo);
            ((ImageButton) v).setImageResource(konjo.getPicture());
            isFirst=false;
            last_pos=v.getId();
        }else{
            if(konjo.isValidMove(v.getId())){

                try{
                    vibrator.vibrate(100);
                }catch (NullPointerException e){
                    Log.e(LOG_FRAGMENT,"No vibrattion");
                }
                konjo.setCurrent_position(v.getId());
                table.setField(last_pos, null);
                table.setField(v.getId(), konjo);
                updateUI();
                last_pos=v.getId();
                moves++;
                updateStatus(moves);

                /* If enemy exists check is
                * field is defended or did konjo ate Enemy
                 */
                if( !mEnemyFigure.isEmpty()){
                    int j;
                    if((j=isPlaceCoverd(last_pos))>=0) {                  /*Eat Konjo*/
                        Log.d(LOG_FRAGMENT, "You are eaten");
                        try {
                            vibrator.vibrate(600);
                        } catch (NullPointerException e) {
                            Log.e(LOG_FRAGMENT, "No vibrattion");
                        }
                        eatKonjo(last_pos,j);
                        updateUI();
                    /*Game Over start Dialog*/
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FinishedDialog dialog = FinishedDialog.newInstance(moves, false, getResources().getString(R.string.gameover_dialog_title));
                        dialog.setTargetFragment(MainActivityFragment.this, REQUEST_FINISH);
                        dialog.show(fm, DIALOG_FINISHED);
                        return;
                    }else if((j=isPositionOfEnemy(last_pos))>=0){ /*Konjo ate Enemy figure*/
                        mEnemyFigure.remove(j);
                    }
                }
                /*If it's end of game send score to server
                 *show Dialog what next to do
                 */
                if(table.isFinished()) {
                    try{
                        vibrator.vibrate(600);
                    }catch (NullPointerException e){
                        Log.e(LOG_FRAGMENT,"No vibrattion");
                    }
                    /*Send to server*/
                    PostRequest pr = new PostRequest();
                    HashMap<String,String> hm =new HashMap<String,String>();
                    hm.put("username",((MainActivity)getActivity()).getUsername());
                    hm.put("moves",String.valueOf(moves));
                    hm.put("level",String.valueOf(getLevel()));
                    pr.execute(hm,null,null);
                    /*start Dialog*/
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FinishedDialog dialog;
                    if(getLevel()==3) {
                        dialog   = FinishedDialog.newInstance(moves, false, getResources().getString(R.string.checkmate_dialog_title));
                    }else{
                        dialog = FinishedDialog.newInstance(moves,true,getResources().getString(R.string.checkmate_dialog_title));
                    }
                    dialog.setTargetFragment(MainActivityFragment.this,REQUEST_FINISH);
                    dialog.show(fm,DIALOG_FINISHED);
                }
            }
        }


    }

    public void updateUI(){
        for(int i=0;i<table.getSize();i++) {
            int pic = table.getField_pic(i);
            ((ImageButton) grid.getChildAt(i)).setImageResource(pic);
        }
    }
    private int get_color_field(int c, int r) {
        if (c % 2 == 0) {
            if (r % 2 == 0) {
                return Color.DKGRAY;
            } else {
                return Color.LTGRAY;
            }
        } else {
            if (r % 2 == 0) {
                return Color.LTGRAY;
            } else {
                return Color.DKGRAY;
            }
        }
    }
    private boolean hasNavBar (Resources resources)
    {
        int id = resources.getIdentifier("config_showNavigationBar", "bool", "android");
        return id > 0 && resources.getBoolean(id);
    }

    /*Clear only alive enemies position*/
    private void clearEnemyPosition(){
        for (int i=0;i<mEnemyFigure.size();i++ ) {
            mEnemyFigure.get(i).clearValidPositon();
            mEnemyFigure.get(i).setCurrent_position(mLevel.getEnemyPosition()[i]);
        }
    }
    private void restartGame(){
        int[] pos=mLevel.getEnemyPosition();
        cleanTable();
        konjo.clearValidPositon();
        if(getLevel()>1 ) {
            clearEnemyPosition();
            mEnemyFigure.clear();
            mEnemyFigure= new ArrayList<>(mLevel.getFigure());

            konjo.setCurrent_position(mLevel.getKonjoPosition());
            /*set all array of enemy figures*/
            for (int i=0;i<mLevel.getNumberOfEnemies();i++ ) {
                table.setField(pos[i],mEnemyFigure.get(i));
            }
            table.setField(mLevel.getKonjoPosition(), konjo);
            last_pos=konjo.getCurrent_position();
        }
        if(getLevel()==1) {
            isFirst = true;
        }
        updateUI();
        moves=0;
        updateStatus(moves);
    }
    private void cleanTable(){
        for(int i=0;i<table.getSize();i++) {
            table.setField(i, new Pawn("Pijun"));
        }
    }

    void updateStatus(int m){
        statusButton.setText(new String(getResources().getString(R.string.status_label) + m));
    }

    private void nextLevel(){
        mCurrentLevel++;
    }

    private int getLevel(){
        return mCurrentLevel;
    }
    @Override
    public void answerFromServer(JSONArray jsonArray) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        HighscoreFragment highscore = HighscoreFragment.newInstance(jsonArray,"FROM_GAME");
        highscore.setTargetFragment(MainActivityFragment.this,REQUEST_FINISH);
        FragmentTransaction ft =fm.beginTransaction().replace(R.id.fragment_container,highscore);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
    }

    private class Level {
        private int mPositon;
        private int mLevel;
        private List<Figure> figure;
        private int[] mEnemyPosition;

        public Level(int level, int position) {
            figure = new ArrayList<>();
            mLevel = level;
            mPositon = position;
            mEnemyPosition=new int[mLevel];
            setLevel();
        }
        public int getKonjoPosition(){
            return mPositon;
        }

        public int[] getEnemyPosition(){
            return mEnemyPosition;
        }
        private void setLevel() {
            switch (mLevel) {
                case 1:
                    Log.d(LOG_LEVEL, " level 1");
                    restartGame();
                    figure = null;
                    break;
                case 2:
                    Log.d(LOG_LEVEL, " level 2");
                    cleanTable();
                    konjo.setCurrent_position(mPositon);
                    table.setField(mPositon, konjo);
                    /*if konjo is already on position where bishop can eat him choose another position for bishop*/
                    do{
                        mEnemyPosition[0] = (new Random()).nextInt(table.getSize());
                        figure.add(0, new Lovac("lovac", mEnemyPosition[0]));
                    }while((figure.get(0).isValidMove(mPositon) || mEnemyPosition[0]==mPositon)&& figure.remove(0)!=null);
                    table.setField(figure.get(0).getCurrent_position(), figure.get(0));
                    break;
                case 3:
                    Log.d(LOG_LEVEL, " level 3");
                    cleanTable();
                    konjo.setCurrent_position(mPositon);
                    table.setField(mPositon, konjo);
                    /*if konjo is already on position where bishop can eat him choose another position for bishop*/
                    do{
                        mEnemyPosition[0] = (new Random()).nextInt(table.getSize());
                        figure.add(0,new Lovac("lovac", mEnemyPosition[0]));
                    }while((figure.get(0).isValidMove(mPositon)|| mEnemyPosition[0]==mPositon) && figure.remove(0)!=null);
                    do{
                        mEnemyPosition[1] = (new Random()).nextInt(table.getSize());
                        figure.add(1, new Top("top", mEnemyPosition[1]));
                    }while((figure.get(1).isValidMove(mPositon) || mEnemyPosition[0]==mEnemyPosition[1] ||mEnemyPosition[1]==mPositon)&& figure.remove(1)!=null);
                    table.setField(figure.get(0).getCurrent_position(), figure.get(0));
                    table.setField(figure.get(1).getCurrent_position(), figure.get(1));
                    break;
                default:
                    Log.d(LOG_LEVEL, "unknown level");
                    restartGame();
                    figure = null;
                    break;
            }
            updateUI();
            moves = 0;
            updateStatus(moves);
        }
        public List<Figure> getFigure(){
            return figure;
        }
        public int getNumberOfEnemies(){
            return figure.size();
        }
    }

}
