package uros.konjo.android.konjo;


import android.util.Log;

/**
 * Created by Lazar on 5/21/2016.
 */
public class Lovac extends Figure{


    public Lovac(String name){
        this.name=name;
        this.moveable=true;
       this.picture_figure=R.drawable.bishop;
    }

    public Lovac(String name, int cur_pos){
        this.current_position=cur_pos;
        this.name=name;
        this.moveable=true;
        this.picture_figure=R.drawable.bishop;
        makeValideMoves();
    }

    @Override
    public boolean isMoveable() {
        return moveable;
    }



    /*This function in Lovac will check places
    *to eat Konjo
    * */
    @Override
    protected void makeValideMoves() {

        int pos =this.current_position %4;
        if(pos == 0){
            if(this.current_position-3>0){
                validMoves.add(this.current_position-3);
                if(this.current_position-6>0){
                    validMoves.add(this.current_position-6);
                    if(this.current_position-9>0){
                        validMoves.add(this.current_position-9);
                    }
                }

            }
            if(this.current_position+5<32){
                validMoves.add(this.current_position+5);
                if(this.current_position+10<32){
                    validMoves.add(this.current_position+10);
                    if(this.current_position+15<32){
                        validMoves.add(this.current_position+15);
                    }
                }

            }
        }else if(pos==1){
            if(this.current_position-5>=0) {
                validMoves.add(this.current_position - 5);
            }
            if(this.current_position-3>=0) {
                validMoves.add(this.current_position - 3);
                if (this.current_position - 6 >= 0) {
                    validMoves.add(this.current_position - 6);
                }
            }
            if(this.current_position+3<32) {
                validMoves.add(this.current_position +3);
            }
            if(this.current_position+5<32) {
                validMoves.add(this.current_position +5);
                if (this.current_position +10<32) {
                    validMoves.add(this.current_position +10);
                }
            }
        }else if(pos==2){
            if(this.current_position-3>=0) {
                validMoves.add(this.current_position - 3);
            }
            if(this.current_position-5>=0) {
                validMoves.add(this.current_position - 5);
                if (this.current_position - 10 >= 0) {
                    validMoves.add(this.current_position - 10);
                }
            }
            if(this.current_position+5<32) {
                validMoves.add(this.current_position +5);
            }
            if(this.current_position+3<32) {
                validMoves.add(this.current_position +3);
                if (this.current_position +6<32) {
                    validMoves.add(this.current_position +6);
                }
            }
        }else if(pos==3){
            if(this.current_position-5>0){
                validMoves.add(this.current_position-5);
                if(this.current_position-10>0){
                    validMoves.add(this.current_position-10);
                    if(this.current_position-15>0){
                        validMoves.add(this.current_position-15);
                    }
                }

            }
            if(this.current_position+3<32){
                validMoves.add(this.current_position+3);
                if(this.current_position+6<32){
                    validMoves.add(this.current_position+6);
                    if(this.current_position+9<32){
                        validMoves.add(this.current_position+9);
                    }
                }

            }
        }
        for (Integer i : validMoves.subList(0,validMoves.size())){
            Log.d(LOG_FIGURE,i+" ");
        }
    }


}
