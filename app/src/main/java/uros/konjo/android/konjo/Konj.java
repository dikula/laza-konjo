package uros.konjo.android.konjo;



/**
 * Created by Lazar on 5/21/2016.
 */
public class Konj extends  Figure{

    public Konj(String name){
        this.name=name;
        this.moveable=true;
        this.picture_figure=R.drawable.knight;
    }
    public Konj(String name, int cur_pos){
        this.current_position=cur_pos;
        this.name=name;
        this.moveable=true;
        this.picture_figure=R.drawable.knight;
        makeValideMoves();
    }

    @Override
    public boolean isMoveable() {
        return moveable;
    }


    protected void makeValideMoves(){

        int pos = 4*(this.current_position/4);


        //drugi red gore od konja
        if(pos-8>=0) {
            if (current_position - 9 < pos - 8) {
                validMoves.add(current_position - 7);
            } else {
                validMoves.add(current_position - 9);
            }
            if (current_position - 7 > pos - 5) {
                //nothing already add in previous step
            } else {
                if(current_position%4!=0) {
                    validMoves.add(current_position - 7);
                }
            }
        }
        //prvi red gore od konja
        if(pos-4>=0){
            if (current_position - 2 < pos) {
                validMoves.add(current_position - 2);
            } else {
                validMoves.add(current_position - 6);
            }
        }
        //prvi red dole od konja
        if(pos+4<=28){
            if (current_position + 2 < pos + 4) {
                validMoves.add(current_position + 6);
            } else {
                validMoves.add(current_position + 2);
            }
        }
        //drugi red dole od konja
        if(pos+8<=28) {
            if (current_position + 7 < pos + 8) {
                validMoves.add(current_position + 9);
            } else {
                validMoves.add(current_position + 7);
            }
            if (current_position + 9 > pos + 11) {
                //nothing already add in previous step
            } else {
                if (current_position % 4 != 0) {
                    validMoves.add(current_position + 9);
                }
            }
        }
    }
}
