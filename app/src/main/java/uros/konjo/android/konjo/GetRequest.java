package uros.konjo.android.konjo;

import android.os.AsyncTask;


import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * Created by uros on 6.12.16..
 */
public class GetRequest extends AsyncTask<String, String, JSONArray> {


    public AsyncResponse delegate = null;

    private String convert_stream_to_string(InputStreamReader is) throws UnsupportedEncodingException {

        BufferedReader reader = new BufferedReader(is);
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    @Override
    protected JSONArray doInBackground(String... url) {
        JSONArray jsr = null;
        HttpURLConnection urlConnection=null;
        String contents = "";
        try {
            URL mUrl = new URL(url[0]);

            urlConnection = (HttpURLConnection) mUrl.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

        }catch (MalformedURLException e) {
            e.printStackTrace();
            return new JSONArray();
        } catch (IOException e) {
            e.printStackTrace();
            return new JSONArray();
        }
        try{
            InputStream in=urlConnection.getInputStream();
            InputStreamReader inr = new InputStreamReader(in);
            contents=convert_stream_to_string(inr);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        try {
            jsr = new JSONArray(contents);
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONArray();
        }

        return jsr;
    }

    @Override
    protected void onPostExecute(JSONArray jsonArray) {
        delegate.answerFromServer(jsonArray);
    }


}
