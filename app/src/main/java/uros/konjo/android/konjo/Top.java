package uros.konjo.android.konjo;

/**
 * Created by Corto on 3/30/2017.
 */
public class Top extends Figure {

    public Top(String name){
        this.name=name;
        this.moveable=true;
        this.picture_figure=R.drawable.tower;
    }
    public Top(String name,int cur_pos){
        this.current_position=cur_pos;
        this.name=name;
        this.moveable=true;
        this.picture_figure=R.drawable.tower;
        makeValideMoves();
    }
    @Override
    public boolean isMoveable() {
        return moveable;
    }

    /*This function in Top will check places
    *to eat Konjo
    * */
    @Override
    protected void makeValideMoves() {
        int i;
        //set columns
        int column = this.current_position%4;

        for( i=0;i<32;i++){
            /*place where is Top is not valid*/
            if(i%4==column && i!=this.current_position){
                validMoves.add(i);
            }
        }

        //set rows
        int row = (this.current_position/4) * 4;
        for(i=row; i<row+4;i++) {
           if(i!=this.current_position ) {
               validMoves.add(i);
           }
        }
    }
}
