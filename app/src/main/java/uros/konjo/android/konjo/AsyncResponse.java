package uros.konjo.android.konjo;

import org.json.JSONArray;

/**
 * Created by Corto on 1/14/2017.
 */
public interface AsyncResponse {
    public void answerFromServer(JSONArray jsonArray);
}
