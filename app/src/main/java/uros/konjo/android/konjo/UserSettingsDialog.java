package uros.konjo.android.konjo;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Corto on 1/21/2017.
 */
public class UserSettingsDialog extends DialogFragment implements View.OnClickListener{

    private static final String ARG_USERNAME="username";

    private Button mOk, mCancel;
    private EditText mEditText;
    private String mUsername;


    public static UserSettingsDialog newInstance(String username) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_USERNAME, username);
        UserSettingsDialog fragment = new UserSettingsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_user_settings,null);
        mUsername=(String)getArguments().getSerializable(ARG_USERNAME);
        mEditText=(EditText) v.findViewById(R.id.dialog_username_edit);
        mEditText.setText(mUsername);

        mCancel = (Button) v.findViewById(R.id.dialog_cancel);
        mCancel.setOnClickListener(this);
        mOk = (Button) v.findViewById(R.id.dialog_ok);
        mOk.setOnClickListener(this);
        return new AlertDialog.Builder(getActivity()).setTitle(getResources().getString(R.string.settings_dialog_title)).setView(v).create();
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.dialog_cancel){
            if(mEditText.getText().toString().isEmpty()){
                mUsername="Anonymous";
            }
        }else{
            mUsername=mEditText.getText().toString();
            if(mUsername.isEmpty()){
                mUsername="Anonymous";
            }
        }
        setResult(mUsername);
        dismiss();

    }

    private void setResult(String username) {
        SharedPreferences pref = getActivity().getSharedPreferences(((MainActivity)getActivity()).SETTINGS_PREF, Context.MODE_PRIVATE);
        pref.edit().putString("username",username).apply();
        ((MainActivity) getActivity()).setUsername(username);
    }
}
