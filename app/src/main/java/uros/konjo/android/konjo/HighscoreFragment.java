package uros.konjo.android.konjo;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HighscoreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HighscoreFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_JSON_ARRAY = "list_of_users";
    private static final String ARG_WHO_CALL = "which_app_call";

    private String mParam1;
    private String caller;
    private JSONArray mJsonHighscoreUSers;


    public HighscoreFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment HighscoreFragment.
     */
    public static HighscoreFragment newInstance(JSONArray param1, String param2) {
        HighscoreFragment fragment = new HighscoreFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_JSON_ARRAY, param1.toString());
        args.putString(ARG_WHO_CALL,param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_JSON_ARRAY);
            caller = getArguments().getString(ARG_WHO_CALL);
            try {
                mJsonHighscoreUSers = new JSONArray(mParam1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_highscore, container, false);
        TableLayout tj = (TableLayout)v.findViewById(R.id.highscore_table);
        int i=0;
        try {
            JSONObject jo = mJsonHighscoreUSers.getJSONObject(i);
            while (jo != null) {
                i++;
                TableRow tr = new TableRow(getActivity());
                TableLayout.LayoutParams layoutRow = new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
                tr.setLayoutParams(layoutRow);

                String username = jo.getString("username");
                TextView tvUser = new TextView(getContext());
                tvUser.setText(username);
                tvUser.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,0.33f));
                tvUser.setGravity(Gravity.CENTER);
                int moves = jo.getInt("moves");
                TextView tvMoves = new TextView(getContext());
                tvMoves.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,0.33f));
                tvMoves.setText(String.valueOf(moves));
                tvMoves.setGravity(Gravity.CENTER);
                int level = jo.getInt("level");
                TextView tvLevel = new TextView(getContext());
                tvLevel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,0.33f));
                tvLevel.setText(String.valueOf(level));
                tvLevel.setGravity(Gravity.CENTER);

                tr.addView(tvUser,0);
                tr.addView(tvMoves,1);
                tr.addView(tvLevel,2);
                tj.addView(tr,i,new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                jo = mJsonHighscoreUSers.getJSONObject(i);
            }
        }catch(JSONException e){
            e.fillInStackTrace();
        }
        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(caller.equals("FROM_GAME")) {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            StartFragment start = new StartFragment();
            FragmentTransaction ft = fm.beginTransaction().replace(R.id.fragment_container, start);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.commit();
        }

    }
}
