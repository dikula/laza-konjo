package uros.konjo.android.konjo;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by uros on 24.11.16..
 */



public class PostRequest extends AsyncTask<HashMap<String,String>, String, String> {

    private final String LOG_TAG="POST_REQUEST_TAG";


    @Override
    protected String doInBackground(HashMap<String,String>... args) {
        String result = new String();


        try {
            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost("http://198.211.126.94:8111/add");
  //          HttpPost httpPost = new HttpPost(mData.get("url"));

            String json = new String();

            // 3. build jsonObject
            JSONObject jsonObject = new JSONObject();

            Log.d( LOG_TAG, args[0].get("username"));
            jsonObject.accumulate("username", args[0].get("username"));
            Log.d( LOG_TAG, args[0].get("moves"));
            jsonObject.accumulate("moves", args[0].get("moves"));
            Log.d( LOG_TAG, args[0].get("level"));
            jsonObject.accumulate("level", args[0].get("level"));

            // 4. convert JSONObject to JSON to String
            json = jsonObject.toString();

            // ** Alternative way to convert Person object to JSON string usin Jackson Lib
            //ObjectMapper mapper = new ObjectMapper();
            //json = mapper.writeValueAsString(person);

            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);

            // 6. set httpPost Entity
            httpPost.setEntity(se);

            // 7. Set some headers to inform server about the type of the content
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            result = EntityUtils.toString(httpResponse.getEntity());
            Log.i("RESPONSEEEEEEEEE", result);

            JSONObject jsonObj = new JSONObject(result);
            return jsonObj.toString();
            /*
            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();
//            // 10. convert inputstream to string
            if(inputStream != null)
                result = inputStream.read();
            else
                result = "Did not work!";
*/
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        // 11. return result
        return "";
    }

}
