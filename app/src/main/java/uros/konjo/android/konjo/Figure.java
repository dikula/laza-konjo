package uros.konjo.android.konjo;



import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lazar on 5/20/2016.
 */
public abstract class Figure {

    protected final String LOG_FIGURE="FIGURE_LOG";

    protected String name;
    protected int current_position;
    protected boolean moveable;
    protected List<Integer> validMoves=new ArrayList<Integer>(32);
    protected int picture_figure;

    public abstract boolean isMoveable();
    protected abstract void makeValideMoves();


    public  void clearValidPositon(){
        this.validMoves.clear();
    }
    public boolean isValidMove(int position) {

        for(int i=0; i< this.validMoves.size() ; i++){
            if(position==this.validMoves.get(i)){
                return true;
            }
        }
        return false;
    }

    public String getName() {  return name;   }
    public int getCurrent_position() {
        return current_position;
    }

    public void setCurrent_position(int current_position) {
        this.current_position = current_position;
        this.clearValidPositon();
        makeValideMoves();
    }

    public int getPicture() {
        return picture_figure;
    }
}
