package uros.konjo.android.konjo;

import android.util.Log;
import android.widget.Toast;

import java.util.HashMap;

/**
 * Created by Corto on 5/20/2016.
 * Table is singleton class. Only once create chess table
 */
public class Table {
    private final int width=4;
    private final int height=8;
    private boolean finished;
    private HashMap<Integer,Figure> fields;

    private static Table sTable=null;

    public static Table makeTable(){
        if(sTable==null){
            sTable= new Table();
        }
        return sTable;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public  Figure getField(int i) {
        return fields.get(i);
    }

    public void setField(int field,  Figure f ){
        if(f==null){
            fields.remove(field);
        }
        fields.put(field, f);
    }
    public int getSize(){ return width*height;}

    public boolean isFinished() {
        for(int i=0;i<width*height;i++){
            if(fields.get(i)!=null && !fields.get(i).isMoveable()){
                return false;
            }
        }
        return true;
    }
    public int getField_pic(int f) {
        if(sTable.getField(f)==null){
            return android.R.color.transparent;
        }
        return sTable.getField(f).getPicture();
    }


    private Table() {
        finished = false;
        fields=new HashMap<>(width*height);
        for (int i = 0; i < width * height; i++){
            Pawn p = new Pawn("pijun",i);
            fields.put(i,p);
        }
    }
}
