package uros.konjo.android.konjo;

import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.concurrent.TimeUnit;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Don't do this! This is just so cold launches take some time
        //SystemClock.sleep(TimeUnit.SECONDS.toMillis(2));
       Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
